microservice_base

微服务基础框架

- SpringBoot   基于Spring+SpringMvc自动配置框架
- Mybatis      ORM框架
- Dubbo        RPC框架
- Nacos        配置和注册中心
- Seata        分布式事务框架



软件架构说明
 - 使用Nacos作为微服务注册和配置中心，Dubbo服务之间相互调用，对订单和库存服务实现分布式事务控制。


安装教程

1.  安装Nacos-server
2.  安装Seata-server

使用说明

1.  启动Nacos-server
2.  启动Seata-server

